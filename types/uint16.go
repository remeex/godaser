package types

import "gitlab.com/remeex/godaser/serializable"

type DUInt16 struct {
	Value uint16
}

func (d *DUInt16) Serialize(out *serializable.Serializer) error {
	return out.WriteUInt16(d.Value)
}
func (d *DUInt16) Deserialize(in *serializable.Deserializer) error {
	return in.ReadUInt16Ptr(&d.Value)
}
func (d *DUInt16) UInt16() uint16 {
	return d.Value
}
