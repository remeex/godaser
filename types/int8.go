package types

import "gitlab.com/remeex/godaser/serializable"

type DInt8 struct {
	Value int8
}

func (d *DInt8) Serialize(out *serializable.Serializer) error {
	return out.WriteInt8(d.Value)
}
func (d *DInt8) Deserialize(in *serializable.Deserializer) error {
	return in.ReadInt8Ptr(&d.Value)
}
func (d *DInt8) Int8() int8 {
	return d.Value
}
