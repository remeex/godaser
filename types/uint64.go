package types

import "gitlab.com/remeex/godaser/serializable"

type DUInt64 struct {
	Value uint64
}

func (d *DUInt64) Serialize(out *serializable.Serializer) error {
	return out.WriteUInt64(d.Value)
}
func (d *DUInt64) Deserialize(in *serializable.Deserializer) error {
	return in.ReadUInt64Ptr(&d.Value)
}
func (d *DUInt64) UInt64() uint64 {
	return d.Value
}
