package types

import "gitlab.com/remeex/godaser/serializable"

type DBool struct {
	Value bool
}

func (d *DBool) Serialize(out *serializable.Serializer) error {
	return out.WriteBool(d.Value)
}
func (d *DBool) Deserialize(in *serializable.Deserializer) error {
	return in.ReadBoolPtr(&d.Value)
}
func (d *DBool) Bool() bool {
	return d.Value
}
