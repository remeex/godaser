package types

import "gitlab.com/remeex/godaser/serializable"

type DUInt8 struct {
	Value uint8
}

func (d *DUInt8) Serialize(out *serializable.Serializer) error {
	return out.WriteUInt8(d.Value)
}
func (d *DUInt8) Deserialize(in *serializable.Deserializer) error {
	return in.ReadUInt8Ptr(&d.Value)
}
func (d *DUInt8) UInt8() uint8 {
	return d.Value
}
