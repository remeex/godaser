package types

import "gitlab.com/remeex/godaser/serializable"

type DUInt32 struct {
	Value uint32
}

func (d *DUInt32) Serialize(out *serializable.Serializer) error {
	return out.WriteUInt32(d.Value)
}
func (d *DUInt32) Deserialize(in *serializable.Deserializer) error {
	return in.ReadUInt32Ptr(&d.Value)
}
func (d *DUInt32) UInt32() uint32 {
	return d.Value
}
