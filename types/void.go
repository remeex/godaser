package types

import "gitlab.com/remeex/godaser/serializable"

type DVoid struct{}

func (*DVoid) Serialize(_ *serializable.Serializer) error {
	return nil
}

func (*DVoid) Deserialize(_ *serializable.Deserializer) error {
	return nil
}
