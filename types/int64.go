package types

import "gitlab.com/remeex/godaser/serializable"

type DInt64 struct {
	Value int64
}

func (d *DInt64) Serialize(out *serializable.Serializer) error {
	return out.WriteInt64(d.Value)
}
func (d *DInt64) Deserialize(in *serializable.Deserializer) error {
	return in.ReadInt64Ptr(&d.Value)
}
func (d *DInt64) Int64() int64 {
	return d.Value
}
