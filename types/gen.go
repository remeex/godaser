package types

//go:generate go run ../_tools/dtypegen.go Int8
//go:generate go run ../_tools/dtypegen.go Int16
//go:generate go run ../_tools/dtypegen.go Int32
//go:generate go run ../_tools/dtypegen.go Int64
//go:generate go run ../_tools/dtypegen.go UInt8
//go:generate go run ../_tools/dtypegen.go UInt16
//go:generate go run ../_tools/dtypegen.go UInt32
//go:generate go run ../_tools/dtypegen.go UInt64
//go:generate go run ../_tools/dtypegen.go Bool
//go:generate go run ../_tools/dtypegen.go String
