package types

import "gitlab.com/remeex/godaser/serializable"

type DString struct {
	Value string
}

func (d *DString) Serialize(out *serializable.Serializer) error {
	return out.WriteString(d.Value)
}
func (d *DString) Deserialize(in *serializable.Deserializer) error {
	return in.ReadStringPtr(&d.Value)
}
func (d *DString) String() string {
	return d.Value
}
