package types

import "gitlab.com/remeex/godaser/serializable"

type DInt16 struct {
	Value int16
}

func (d *DInt16) Serialize(out *serializable.Serializer) error {
	return out.WriteInt16(d.Value)
}
func (d *DInt16) Deserialize(in *serializable.Deserializer) error {
	return in.ReadInt16Ptr(&d.Value)
}
func (d *DInt16) Int16() int16 {
	return d.Value
}
