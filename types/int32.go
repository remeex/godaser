package types

import "gitlab.com/remeex/godaser/serializable"

type DInt32 struct {
	Value int32
}

func (d *DInt32) Serialize(out *serializable.Serializer) error {
	return out.WriteInt32(d.Value)
}
func (d *DInt32) Deserialize(in *serializable.Deserializer) error {
	return in.ReadInt32Ptr(&d.Value)
}
func (d *DInt32) Int32() int32 {
	return d.Value
}
