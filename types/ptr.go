package types

import "gitlab.com/remeex/godaser/serializable"

type DPtr struct {
	Value uintptr
}

func (d *DPtr) Serialize(out *serializable.Serializer) error {
	return out.WritePointer(d.Value)
}

func (d *DPtr) Deserialize(in *serializable.Deserializer) error {
	return in.ReadPointerPtr(&d.Value)
}
