package main

import (
	"errors"
	"go/ast"
	"go/printer"
	"go/token"
	"os"
	"strings"
)

type TypeInfo struct {
	TypeName, DSTypeName string
}

func DTypeOf(ti *TypeInfo) *ast.File {
	structName := ast.NewIdent("D" + ti.DSTypeName)
	recvName := ast.NewIdent("d")
	recvType := &ast.StarExpr{X: structName}
	recv := &ast.FieldList{List: []*ast.Field{
		{
			Names: []*ast.Ident{recvName},
			Type:  recvType,
		},
	}}
	valueName := ast.NewIdent("Value")
	valueType := ast.NewIdent(ti.TypeName)
	valueAccess := &ast.SelectorExpr{
		X:   recvName,
		Sel: valueName,
	}

	structure := &ast.GenDecl{
		Tok: token.TYPE,
		Specs: []ast.Spec{
			&ast.TypeSpec{
				Name: structName,
				Type: &ast.StructType{
					Fields: &ast.FieldList{List: []*ast.Field{
						{
							Names: []*ast.Ident{valueName},
							Type:  valueType,
						},
					}},
				},
			},
		},
	}

	serializer := &ast.FuncDecl{
		Recv: recv,
		Name: ast.NewIdent("Serialize"),
		Type: &ast.FuncType{
			Params: &ast.FieldList{List: []*ast.Field{
				{
					Names: []*ast.Ident{ast.NewIdent("out")},
					Type: &ast.StarExpr{X: &ast.SelectorExpr{
						X:   ast.NewIdent("serializable"),
						Sel: ast.NewIdent("Serializer"),
					}},
				},
			}},
			Results: &ast.FieldList{List: []*ast.Field{
				{Type: ast.NewIdent("error")},
			}},
		},
		Body: &ast.BlockStmt{List: []ast.Stmt{
			&ast.ReturnStmt{Results: []ast.Expr{
				&ast.CallExpr{
					Fun: &ast.SelectorExpr{
						X:   ast.NewIdent("out"),
						Sel: ast.NewIdent("Write" + ti.DSTypeName),
					},
					Args: []ast.Expr{
						valueAccess,
					},
				},
			}},
		}},
	}

	deserializer := &ast.FuncDecl{
		Recv: recv,
		Name: ast.NewIdent("Deserialize"),
		Type: &ast.FuncType{
			Params: &ast.FieldList{List: []*ast.Field{
				{
					Names: []*ast.Ident{ast.NewIdent("in")},
					Type: &ast.StarExpr{X: &ast.SelectorExpr{
						X:   ast.NewIdent("serializable"),
						Sel: ast.NewIdent("Deserializer"),
					}},
				},
			}},
			Results: &ast.FieldList{List: []*ast.Field{
				{Type: ast.NewIdent("error")},
			}},
		},
		Body: &ast.BlockStmt{List: []ast.Stmt{
			&ast.ReturnStmt{Results: []ast.Expr{
				&ast.CallExpr{
					Fun: &ast.SelectorExpr{
						X:   ast.NewIdent("in"),
						Sel: ast.NewIdent("Read" + ti.DSTypeName + "Ptr"),
					},
					Args: []ast.Expr{
						&ast.UnaryExpr{
							Op: token.AND,
							X:  valueAccess,
						},
					},
				},
			}},
		}},
	}

	caster := &ast.FuncDecl{
		Recv: recv,
		Name: ast.NewIdent(ti.DSTypeName),
		Type: &ast.FuncType{
			Params: &ast.FieldList{List: []*ast.Field{}},
			Results: &ast.FieldList{List: []*ast.Field{
				{Type: valueType},
			}},
		},
		Body: &ast.BlockStmt{List: []ast.Stmt{
			&ast.ReturnStmt{Results: []ast.Expr{
				valueAccess,
			}},
		}},
	}

	return &ast.File{
		Name: ast.NewIdent("types"),
		Decls: []ast.Decl{
			&ast.GenDecl{
				Tok: token.IMPORT,
				Specs: []ast.Spec{
					&ast.ImportSpec{
						Path: &ast.BasicLit{
							Kind:  token.STRING,
							Value: `"gitlab.com/remeex/godaser/serializable"`,
						},
					},
				},
			},
			structure,
			serializer,
			deserializer,
			caster,
		},
	}
}

func parseTypeInfo() (*TypeInfo, error) {
	ti := new(TypeInfo)
	for _, it := range os.Args[1:] {
		switch {
		case len(ti.DSTypeName) == 0:
			ti.DSTypeName = it
		case len(ti.TypeName) == 0:
			ti.TypeName = it
		default:
			break
		}
	}
	if len(ti.DSTypeName) == 0 {
		return nil, errors.New("type name not specified")
	}
	if len(ti.TypeName) == 0 {
		ti.TypeName = strings.ToLower(ti.DSTypeName)
	}
	return ti, nil
}

func generate(ti *TypeInfo) error {
	f := DTypeOf(ti)
	fset := token.NewFileSet()
	out, err := os.Create(ti.TypeName + ".go")
	if err == nil {
		err = printer.Fprint(out, fset, f)
		if cerr := out.Close(); err == nil {
			err = cerr
		} else if cerr != nil {
			err = errors.New(err.Error() + "\n" + cerr.Error())
		}
	}
	return err
}

func execute() error {
	if ti, err := parseTypeInfo(); err != nil {
		return err
	} else {
		return generate(ti)
	}
}

func main() {
	if err := execute(); err != nil {
		println(err.Error())
		os.Exit(1)
	}
}
