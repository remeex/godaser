package serializable

import "io"

type Serializable interface {
	Serialize(out *Serializer) error
	Deserialize(in *Deserializer) error
}

type Serializer struct {
	out io.Writer
}

type Deserializer struct {
	in io.Reader
}
