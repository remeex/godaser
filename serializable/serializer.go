package serializable

import (
	"io"
)

func NewSerializer(out io.Writer) *Serializer {
	if out == nil {
		return nil
	}
	return &Serializer{out: out}
}

func (s *Serializer) Close() error {
	if c, ok := s.out.(io.Closer); ok {
		return c.Close()
	}
	return nil
}

func (s *Serializer) WriteBytes(value []byte) error {
	if n, err := s.out.Write(value); err != nil {
		return err
	} else if n < len(value) {
		return io.ErrShortWrite
	} else {
		return nil
	}
}

func (s *Serializer) WriteByte(value byte) error {
	return s.WriteBytes([]byte{value})
}

func (s *Serializer) WriteBool(value bool) error {
	if value {
		return s.WriteByte(1)
	} else {
		return s.WriteByte(0)
	}
}

func (s *Serializer) WriteUInt8(value uint8) error {
	return s.WriteByte(value)
}

func (s *Serializer) WriteInt8(value int8) error {
	return s.WriteUInt8(uint8(value))
}

func (s *Serializer) WriteUInt16(value uint16) error {
	return s.WriteBytes([]byte{
		byte((value >> 8) & 0xff),
		byte(value & 0xff),
	})
}

func (s *Serializer) WriteInt16(value int16) error {
	return s.WriteUInt16(uint16(value))
}

func (s *Serializer) WriteUInt32(value uint32) error {
	return s.WriteBytes([]byte{
		byte((value >> 24) & 0xff),
		byte((value >> 8) & 0xff),
		byte((value >> 16) & 0xff),
		byte(value & 0xff),
	})
}

func (s *Serializer) WriteInt32(value int32) error {
	return s.WriteUInt32(uint32(value))
}

func (s *Serializer) WriteUInt64(value uint64) error {
	return s.WriteBytes([]byte{
		byte((value >> 56) & 0xff),
		byte((value >> 40) & 0xff),
		byte((value >> 24) & 0xff),
		byte((value >> 8) & 0xff),
		byte((value >> 48) & 0xff),
		byte((value >> 32) & 0xff),
		byte((value >> 16) & 0xff),
		byte(value & 0xff),
	})
}

func (s *Serializer) WriteInt64(value int64) error {
	return s.WriteUInt64(uint64(value))
}

func (s *Serializer) WriteUInt(value uint) error {
	return s.WriteUInt64(uint64(value))
}

func (s *Serializer) WriteInt(value int) error {
	return s.WriteUInt(uint(value))
}

func (s *Serializer) WritePointer(value uintptr) error {
	return s.WriteUInt(uint(value))
}

func (s *Serializer) WriteString(value string) error {
	b := []byte(value)
	if err := s.WriteInt(len(b)); err != nil {
		return err
	}
	return s.WriteBytes(b)
}

func (s *Serializer) WriteSerializable(value Serializable) error {
	return value.Serialize(s)
}
