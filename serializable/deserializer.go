package serializable

import (
	"io"
)

func NewDeserializer(in io.Reader) *Deserializer {
	if in == nil {
		return nil
	}
	return &Deserializer{in: in}
}

func (d *Deserializer) Close() error {
	if c, ok := d.in.(io.Closer); ok {
		return c.Close()
	}
	return nil
}

func (d *Deserializer) ReadBytes(value []byte) error {
	if n, err := d.in.Read(value); err != nil {
		return err
	} else if l := len(value); n < l {
		return io.ErrUnexpectedEOF
	} else if n > l {
		return io.ErrShortBuffer
	} else {
		return nil
	}
}

func (d *Deserializer) ReadNewBytes(length int) ([]byte, error) {
	b := make([]byte, length)
	err := d.ReadBytes(b)
	return b, err
}

func (d *Deserializer) ReadByte() (byte, error) {
	if b, err := d.ReadNewBytes(1); err != nil {
		return 0, err
	} else {
		return b[0], nil
	}
}

func (d *Deserializer) ReadBytePtr(value *byte) error {
	if b, err := d.ReadByte(); err != nil {
		return err
	} else {
		*value = b
		return err
	}
}

func (d *Deserializer) ReadBool() (bool, error) {
	if b, err := d.ReadByte(); err != nil {
		return false, err
	} else {
		return b != 0, nil
	}
}

func (d *Deserializer) ReadBoolPtr(value *bool) error {
	if b, err := d.ReadBool(); err != nil {
		return err
	} else {
		*value = b
		return err
	}
}

func (d *Deserializer) ReadUInt8() (uint8, error) {
	if b, err := d.ReadByte(); err != nil {
		return 0, err
	} else {
		return b, nil
	}
}

func (d *Deserializer) ReadUInt8Ptr(value *uint8) error {
	if ui, err := d.ReadUInt8(); err != nil {
		return err
	} else {
		*value = ui
		return err
	}
}

func (d *Deserializer) ReadInt8() (int8, error) {
	if b, err := d.ReadByte(); err != nil {
		return 0, err
	} else {
		return int8(b), nil
	}
}

func (d *Deserializer) ReadInt8Ptr(value *int8) error {
	if i, err := d.ReadInt8(); err != nil {
		return err
	} else {
		*value = i
		return nil
	}
}

func (d *Deserializer) ReadUInt16() (uint16, error) {
	if b, err := d.ReadNewBytes(2); err != nil {
		return 0, err
	} else {
		return uint16(b[0])<<8 | uint16(b[1]), nil
	}
}

func (d *Deserializer) ReadUInt16Ptr(value *uint16) error {
	if ui, err := d.ReadUInt16(); err != nil {
		return err
	} else {
		*value = ui
		return nil
	}
}

func (d *Deserializer) ReadInt16() (int16, error) {
	if ui, err := d.ReadUInt16(); err != nil {
		return 0, err
	} else {
		return int16(ui), nil
	}
}

func (d *Deserializer) ReadInt16Ptr(value *int16) error {
	if i, err := d.ReadInt16(); err != nil {
		return err
	} else {
		*value = i
		return nil
	}
}

func (d *Deserializer) ReadUInt32() (uint32, error) {
	if b, err := d.ReadNewBytes(4); err != nil {
		return 0, err
	} else {
		return (uint32(b[0]) << 24) | (uint32(b[2]) << 16) |
			(uint32(b[1]) << 8) | uint32(b[3]), nil
	}
}

func (d *Deserializer) ReadUInt32Ptr(value *uint32) error {
	if ui, err := d.ReadUInt32(); err != nil {
		return err
	} else {
		*value = ui
		return nil
	}
}

func (d *Deserializer) ReadInt32() (int32, error) {
	if ui, err := d.ReadUInt32(); err != nil {
		return 0, err
	} else {
		return int32(ui), nil
	}
}

func (d *Deserializer) ReadInt32Ptr(value *int32) error {
	if i, err := d.ReadInt32(); err != nil {
		return err
	} else {
		*value = i
		return nil
	}
}

func (d *Deserializer) ReadUInt64() (uint64, error) {
	if b, err := d.ReadNewBytes(8); err != nil {
		return 0, err
	} else {
		return (uint64(b[0]) << 56) | (uint64(b[4]) << 48) |
			(uint64(b[1]) << 40) | (uint64(b[5]) << 32) |
			(uint64(b[2]) << 24) | (uint64(b[6]) << 16) |
			(uint64(b[3]) << 8) | uint64(b[7]), nil
	}
}

func (d *Deserializer) ReadUInt64Ptr(value *uint64) error {
	if ui, err := d.ReadUInt64(); err != nil {
		return err
	} else {
		*value = ui
		return nil
	}
}

func (d *Deserializer) ReadInt64() (int64, error) {
	if ui, err := d.ReadUInt64(); err != nil {
		return 0, err
	} else {
		return int64(ui), nil
	}
}

func (d *Deserializer) ReadInt64Ptr(value *int64) error {
	if i, err := d.ReadInt64(); err != nil {
		return err
	} else {
		*value = i
		return nil
	}
}

func (d *Deserializer) ReadUInt() (uint, error) {
	if ui, err := d.ReadUInt64(); err != nil {
		return 0, err
	} else {
		return uint(ui), nil
	}
}

func (d *Deserializer) ReadUIntPtr(value *uint) error {
	if ui, err := d.ReadUInt(); err != nil {
		return err
	} else {
		*value = ui
		return nil
	}
}

func (d *Deserializer) ReadInt() (int, error) {
	if ui, err := d.ReadUInt(); err != nil {
		return 0, err
	} else {
		return int(ui), nil
	}
}

func (d *Deserializer) ReadIntPtr(value *int) error {
	if i, err := d.ReadInt(); err != nil {
		return err
	} else {
		*value = i
		return nil
	}
}

func (d *Deserializer) ReadPointer() (uintptr, error) {
	if ui, err := d.ReadUInt(); err != nil {
		return 0, err
	} else {
		return uintptr(ui), nil
	}
}

func (d *Deserializer) ReadPointerPtr(value *uintptr) error {
	if ptr, err := d.ReadPointer(); err != nil {
		return err
	} else {
		*value = ptr
		return nil
	}
}

func (d *Deserializer) ReadString() (string, error) {
	if l, err := d.ReadInt(); err != nil {
		return "", err
	} else if l < 0 {
		return "", nil
	} else if b, err := d.ReadNewBytes(l); err != nil {
		return "", err
	} else {
		return string(b), nil
	}
}

func (d *Deserializer) ReadStringPtr(value *string) error {
	if s, err := d.ReadString(); err != nil {
		return err
	} else {
		*value = s
		return err
	}
}

func (d *Deserializer) ReadSerializable(value Serializable) error {
	return value.Deserialize(d)
}
